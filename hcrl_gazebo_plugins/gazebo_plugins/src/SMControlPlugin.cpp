/*
 * Copyright (C) 2012-2014 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <ros/ros.h>
#include <boost/bind.hpp>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <stdio.h>
#include <unistd.h>
#include <sensor_msgs/JointState.h>
#include <nav_msgs/Odometry.h>
#include <shared_memory_interface/shared_memory_interface_ros.hpp>

namespace gazebo
{

#define LOCKSTEP 0
#define LOCKSTEP_TIMEOUT 0.05 //seconds
#define PRINT_STATE_SENT 0
#define PRINT_COMMAND_RECEIVED 0

#define ADD_STICTION 0
#define STICTION_THRESHOLD_VELOCITY 0.1
#define STICTION_THRESHOLD_FORCE 10

  class SMControlPlugin: public ModelPlugin
  {
  public:
    SMControlPlugin() :
        smi("smi")
    {
    }

    ~SMControlPlugin()
    {
    }

    void Load(physics::ModelPtr _parent, sdf::ElementPtr /*_sdf*/)
    {
      // gzmsg << __func__ << ": Method called!\n";

      // Store the pointer to the model
      this->model = _parent;

      // Listen to the update event. This event is broadcasted every simulation iteration.
      this->updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&SMControlPlugin::OnUpdate, this, _1));

      // By default, expose every joint in the robot.
      unsigned long num_joints = this->model->GetJoints().size();

      // Get Gazebo's list of joints
      const gazebo::physics::Joint_V jointListGazebo = this->model->GetJoints();

      // Allocate a vector for holding the joint order in shared memory
      std::vector<std::string> jointOrderNameInSM;

      // If another value for the number of joints is specified on the ROS parameter server, use it.
      this->rosNode = new ros::NodeHandle("");
      int numDOFs;
      if(this->rosNode->getParam("/SMControlPlugin/numDOFs", numDOFs))
      {
        ROS_INFO_STREAM("Using alternate number of DOFs: " << numDOFs);
        num_joints = numDOFs;

        XmlRpc::XmlRpcValue jointOrderList;
        if(this->rosNode->getParam("/SMControlPlugin/JointOrder", jointOrderList))
        {
          ROS_ASSERT(jointOrderList.getType() == XmlRpc::XmlRpcValue::TypeArray);

          // Store the joint order in a std::vector<std::string> object
          for(int32_t ii = 0; ii < jointOrderList.size(); ++ii)
          {
            ROS_ASSERT(jointOrderList[ii].getType() == XmlRpc::XmlRpcValue::TypeString);

            // ROS_INFO_STREAM("Index " << ii << ", joint name: " << static_cast<std::string>(jointOrderList[ii]));
            jointOrderNameInSM.push_back(static_cast<std::string>(jointOrderList[ii]));
          }

          // Find the Gazebo index for each joint and store it in joint_index_sm_to_gazebo_map
          for(size_t ii = 0; ii < jointOrderNameInSM.size(); ii++)
          {
            bool jointFound = false;
            for(size_t jj = 0; !jointFound && jj < jointListGazebo.size(); jj++)
            {
              if(jointListGazebo[jj]->GetName().compare(jointOrderNameInSM[ii]) == 0)
              {
                std::cerr << "Shared memory joint " << ii << " (" << jointOrderNameInSM[ii] << ") is Gazebo joint index " << jj << std::endl;

                joint_index_sm_to_gazebo_map.push_back(jj);
                jointFound = true;
              }
            }

            if(!jointFound)
            {
              ROS_ERROR_STREAM("Unable to get Gazebo index for joint " << ii << " named " << jointOrderNameInSM[ii]);
              ros::shutdown();
              return;
            }
          }
        }
        else
        {
          ROS_ERROR("Failed to get joint order.  Ensure parameter \"SMControlPlugin/JointOrder\" is set!");
          ros::shutdown();
          return;
        }
      }
      else
      {
        // Assume the SM index is the same as the Gazebo index.

        std::stringstream ss;

        for(size_t ii = 0; ii < this->model->GetJoints().size(); ii++)
        {
          joint_index_sm_to_gazebo_map.push_back(ii);
          jointOrderNameInSM.push_back(jointListGazebo[ii]->GetName());
          ss << ii << ": " << jointListGazebo[ii]->GetName() << "\n";
        }

        std::cerr << "SMControlPlugin: jointOrderNameInSM:\n" << ss.str() << std::endl;
      }

      // If a list of unactuated joints is provided, load it.
      XmlRpc::XmlRpcValue unactuatedJointList;
      std::stringstream ss;
      if(this->rosNode->getParam("/SMControlPlugin/UnactuatedJoints", unactuatedJointList))
      {
        ROS_ASSERT(unactuatedJointList.getType() == XmlRpc::XmlRpcValue::TypeArray);

        // For each joint in Gazebo, if it appears in the unactuated joint list,
        // save a 1 in its position in the unactuated_joint_mask.
        for(size_t jj = 0; jj < jointListGazebo.size(); jj++)
        {
          bool jointIsMasked = false;

          for(int kk = 0; !jointIsMasked && kk < unactuatedJointList.size(); kk++)
          {

            ROS_ASSERT(unactuatedJointList[kk].getType() == XmlRpc::XmlRpcValue::TypeString);

            if(jointListGazebo[jj]->GetName().compare(unactuatedJointList[kk]) == 0)
            {
              std::cerr << "Joint \"" << unactuatedJointList[kk] << "\" (Gazebo index " << jj << ") is unactuated!" << std::endl;

              jointIsMasked = true;
            }
          }

          unactuated_joint_mask.push_back(jointIsMasked? 1 : 0);
          ss << "  - " << jj << ", " << jointListGazebo[jj]->GetName() << ", " << (jointIsMasked? "unactuated" : "actuated") << std::endl;
        }
      }
      else
      {
        // No unactuated joints.  Initialize unactuated_joint_mask to contain all zeros.
        for(size_t jj = 0; jj < jointListGazebo.size(); jj++)
        {
          unactuated_joint_mask.push_back(0);
          ss << "  - " << jj << ", " << jointListGazebo[jj]->GetName() << ", " << "actuated" << std::endl;
        }
      }

      std::cerr << "Unactuated_joint_mask:" << std::endl << ss.str();

      smi.advertiseStringVector("joint_names", jointOrderNameInSM.size());
      smi.publishStringVector("joint_names", jointOrderNameInSM);
      smi.advertiseFloatingPointMatrix("pos_vel_torque_measurement", 3, jointOrderNameInSM.size());
      smi.advertiseFloatingPointVector("rtt", 2); //tx,rx

      std::vector<double> initial_rtt;
      initial_rtt.push_back(0);
      initial_rtt.push_back(0);
      smi.publishFloatingPointVector("rtt", initial_rtt);

      smi.advertiseSerializedROS<nav_msgs::Odometry>("root_link_odom");

      // Root link odometry
      gazebo::physics::Link_V links = this->model->GetLinks();

      nav_msgs::Odometry odom;
      gazebo::math::Pose pelvis_pose = links.at(0)->GetWorldPose();
      gazebo::math::Vector3 pelvis_linear_vel = links.at(0)->GetWorldLinearVel();
      gazebo::math::Vector3 pelvis_angular_vel = links.at(0)->GetWorldAngularVel();
      odom.pose.pose.position.x = pelvis_pose.pos.x;
      odom.pose.pose.position.y = pelvis_pose.pos.y;
      odom.pose.pose.position.z = pelvis_pose.pos.z;

      // Get the orientation
      odom.pose.pose.orientation.w = pelvis_pose.rot.w;
      odom.pose.pose.orientation.x = pelvis_pose.rot.x;
      odom.pose.pose.orientation.y = pelvis_pose.rot.y;
      odom.pose.pose.orientation.z = pelvis_pose.rot.z;

      // Get the velocity
      odom.twist.twist.linear.x = pelvis_linear_vel.x;
      odom.twist.twist.linear.y = pelvis_linear_vel.y;
      odom.twist.twist.linear.z = pelvis_linear_vel.z;
      odom.twist.twist.angular.x = pelvis_angular_vel.x;
      odom.twist.twist.angular.y = pelvis_angular_vel.y;
      odom.twist.twist.angular.z = pelvis_angular_vel.z;

      // Publish the odometry information
      smi.publishSerializedROS<nav_msgs::Odometry>("root_link_odom", odom);
    }

    // Called by the world update start event
    void OnUpdate(const common::UpdateInfo & /*_info*/)
    {
      // Root link odometry
      gazebo::physics::Link_V links = this->model->GetLinks();

      nav_msgs::Odometry odom;
      gazebo::math::Pose pelvis_pose = links.at(0)->GetWorldPose();
      gazebo::math::Vector3 pelvis_linear_vel = links.at(0)->GetWorldLinearVel();
      gazebo::math::Vector3 pelvis_angular_vel = links.at(0)->GetWorldAngularVel();
      odom.pose.pose.position.x = pelvis_pose.pos.x;
      odom.pose.pose.position.y = pelvis_pose.pos.y;
      odom.pose.pose.position.z = pelvis_pose.pos.z;

      // Get the orientation
      odom.pose.pose.orientation.w = pelvis_pose.rot.w;
      odom.pose.pose.orientation.x = pelvis_pose.rot.x;
      odom.pose.pose.orientation.y = pelvis_pose.rot.y;
      odom.pose.pose.orientation.z = pelvis_pose.rot.z;

      // Get the velocity
      odom.twist.twist.linear.x = pelvis_linear_vel.x;
      odom.twist.twist.linear.y = pelvis_linear_vel.y;
      odom.twist.twist.linear.z = pelvis_linear_vel.z;
      odom.twist.twist.angular.x = pelvis_angular_vel.x;
      odom.twist.twist.angular.y = pelvis_angular_vel.y;
      odom.twist.twist.angular.z = pelvis_angular_vel.z;

      // Publish the odometry information
      smi.publishSerializedROS<nav_msgs::Odometry>("root_link_odom", odom);

      gazebo::physics::Joint_V joints = this->model->GetJoints();
      std::vector<double> positions;
      std::vector<double> velocities;
      std::vector<double> torques;

      // Get the current joint states
      for(unsigned int ii = 0; ii < joint_index_sm_to_gazebo_map.size(); ii++)
      {
        size_t gazeboIndex = joint_index_sm_to_gazebo_map[ii];

        gazebo::physics::JointWrench gazebo_wrench = joints[gazeboIndex]->GetForceTorque(0);
        math::Vector3 axis = joints[gazeboIndex]->GetLocalAxis(0);
        double position = joints[gazeboIndex]->GetAngle(0).Radian();
        double velocity = joints[gazeboIndex]->GetVelocity(0);
        double torque = axis.x * gazebo_wrench.body2Torque.x + axis.y * gazebo_wrench.body2Torque.y + axis.z * gazebo_wrench.body2Torque.z;

        if(std::isnan(position) || std::isnan(position))
        {
          ROS_WARN("NAN or INF position found");
          position = 0;
        }
        if(std::isnan(velocity) || std::isnan(velocity))
        {
          ROS_WARN("NAN or INF velocity found");
          velocity = 0;
        }
        if(std::isnan(torque) || std::isnan(torque))
        {
          ROS_WARN("NAN or INF torque found");
          torque = 0;
        }

        positions.push_back(position);
        velocities.push_back(velocity);
        torques.push_back(torque);
      }

      // Prints the robot state being saved into shared memory
      // std::stringstream ss;
      // for(unsigned int ii = 0; ii < joint_index_sm_to_gazebo_map.size(); ii++)
      // {
      // size_t gazeboIndex = joint_index_sm_to_gazebo_map[ii];

      // ss << "  Joint: " << joints[gazeboIndex]->GetName() << ", position: " << positions[ii]
      //    << ", velocity: " << velocities[ii] << ", torque: " << torques[ii] << std::endl;
      // }
      // std::cerr << "SMControlPlugin: Sending Robot State:" << std::endl << ss.str();

#if PRINT_STATE_SENT
      std::stringstream stateSendss;
      stateSendss << "SMControlPlugin: Sending:\n"
      << " - positions: [";

      for (size_t ii = 0; ii < joints.size(); ii++)
      {
        stateSendss << positions[ii];
        if (ii < joints.size() - 1)
        stateSendss << ", ";
      }

      stateSendss << "]\n"
      << " - velocities: [";

      for (size_t ii = 0; ii < joints.size(); ii++)
      {
        stateSendss << velocities[ii];
        if (ii < joints.size() - 1)
        stateSendss << ", ";
      }

      stateSendss << "]\n"
      << " - torques: [";

      for (size_t ii = 0; ii < joints.size(); ii++)
      {
        stateSendss << torques[ii];
        if (ii < joints.size() - 1)
        stateSendss << ", ";
      }

      stateSendss << "]";

      std::cerr << stateSendss.str() << std::endl;
#endif

      // overwrite actual measurements with test data
      // for(int i = 0; i < positions.size(); i++)
      // {
      //     positions[i] = i;
      //     velocities[i] = i * 10.0;
      //     torques[i] = i * 100.0;
      // }

      std::vector<double> pos_vel_torque_measurements;
      pos_vel_torque_measurements.insert(pos_vel_torque_measurements.end(), positions.begin(), positions.end());
      pos_vel_torque_measurements.insert(pos_vel_torque_measurements.end(), velocities.begin(), velocities.end());
      pos_vel_torque_measurements.insert(pos_vel_torque_measurements.end(), torques.begin(), torques.end());
      smi.publishFloatingPointMatrix("pos_vel_torque_measurement", pos_vel_torque_measurements);

      std::vector<double> torque_cmds;
      if(LOCKSTEP)
      {
        if(!smi.waitForFloatingPointVector("torque_command", torque_cmds, LOCKSTEP_TIMEOUT))
        {
          return;
        }
      }
      else
      {
        if(!smi.getCurrentFloatingPointVector("torque_command", torque_cmds, LOCKSTEP_TIMEOUT)) //TODO: remove magic number
        {
          return;
        }
      }

      // Issue the new torque commands to Gazebo
#if PRINT_COMMAND_RECEIVED
      std::stringstream ssRead;
#endif

      for(unsigned int ii = 0; ii < joint_index_sm_to_gazebo_map.size(); ii++)
      {
        size_t gazeboIndex = joint_index_sm_to_gazebo_map[ii];

        // Only issue commands to actuated joints
        if(unactuated_joint_mask[gazeboIndex] == 0)
        {
          if(ADD_STICTION && (fabs(velocities[ii]) < STICTION_THRESHOLD_VELOCITY))
          {
            if(fabs(torque_cmds[ii]) > STICTION_THRESHOLD_FORCE)
            {
              joints[gazeboIndex]->SetForce(0, torque_cmds[ii]);
            }
            else
            {
              velocities[ii] = 0;
              joints[gazeboIndex]->SetVelocity(0, 0.0);
              joints[gazeboIndex]->SetForce(0, 0.0);
            }
          }
          else
          {
            joints[gazeboIndex]->SetForce(0, torque_cmds[ii]);
          }

#if PRINT_COMMAND_RECEIVED
          ssRead << "  Joint: " << joints[gazeboIndex]->GetName() << ", torque: " << torque_cmds[ii] << std::endl;
#endif
        }
      }

#if PRINT_COMMAND_RECEIVED
      std::cerr << "SMControlPlugin: Read command:" << std::endl << ssRead.str();
#endif

      // Reflect the RTT sequence number so the round-trip time can be computed
      std::vector<double> current_rtt;
      smi.getCurrentFloatingPointVector("rtt", current_rtt);
      assert(current_rtt.size() == 2);
      current_rtt[1] = current_rtt[0]; //rx <= tx
      smi.publishFloatingPointVector("rtt", current_rtt);
    }

  private:
    // Pointer to the model
    physics::ModelPtr model;

    // Pointer to the update event connection
    event::ConnectionPtr updateConnection;

    shared_memory_interface::SharedMemoryInterfaceROS smi;

    // Maps the index of the joint within the shared memory
    // to the index of the joint in gazebo.
    std::vector<size_t> joint_index_sm_to_gazebo_map;

    // Contains a 1 if the joint is masked b/c it is unactuated, 0 otherwise
    // It is indexed by Gazebo joint order.
    std::vector<size_t> unactuated_joint_mask;

    ros::NodeHandle * rosNode;
  };

// Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(SMControlPlugin)

} // namespace gazebo
