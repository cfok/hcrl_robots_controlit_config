#!/bin/bash

echo Creating directory \'urdf\'
mkdir -p dreamer_no_left_arm_controlit/urdf

echo Generating dreamer_no_left_arm_controlit/urdf/dreamer_no_left_arm_gazebo.urdf
rosrun xacro xacro.py dreamer_no_left_arm_controlit/xacro/dreamer_no_left_arm_gazebo.xacro -o dreamer_no_left_arm_controlit/urdf/dreamer_no_left_arm_gazebo.urdf

echo Generating dreamer_no_left_arm_controlit/urdf/dreamer_no_left_arm_controlit.urdf
rosrun xacro xacro.py dreamer_no_left_arm_controlit/xacro/dreamer_no_left_arm_controlit.xacro -o dreamer_no_left_arm_controlit/urdf/dreamer_no_left_arm_controlit.urdf

echo Done!
